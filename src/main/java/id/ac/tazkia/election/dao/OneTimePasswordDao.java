package id.ac.tazkia.election.dao;

import id.ac.tazkia.election.entity.OneTimePassword;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface OneTimePasswordDao extends PagingAndSortingRepository<OneTimePassword, String> {
}
