package id.ac.tazkia.election.dao;

import id.ac.tazkia.election.entity.Pendaftar;
import id.ac.tazkia.election.entity.PengalamanWirausaha;
import id.ac.tazkia.election.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PengalamanWirausahaDao extends PagingAndSortingRepository<PengalamanWirausaha, String> {

    Page<PengalamanWirausaha> findByStatusAndPendaftar(StatusRecord statusRecord, Pendaftar pendaftar, Pageable page);

    List<PengalamanWirausaha> findByStatusAndPendaftar(StatusRecord statusRecord, Pendaftar pendaftar);

}
