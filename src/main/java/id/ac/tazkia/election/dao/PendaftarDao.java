package id.ac.tazkia.election.dao;

import id.ac.tazkia.election.entity.Pendaftar;
import id.ac.tazkia.election.entity.StatusRecord;
import id.ac.tazkia.election.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PendaftarDao extends PagingAndSortingRepository<Pendaftar, String> {
    Pendaftar findByStatusAndUser(StatusRecord statusRecord, User user);

    Page<Pendaftar> findByStatusOrderByTanggalInput(StatusRecord statusRecord, Pageable pageable);

    Page<Pendaftar> findByStatusAndUserRoleIdAndStatusVerifikasiOrderByTanggalInput(StatusRecord statusRecord, String idRole, StatusRecord verifikasi,Pageable pageable);
    Page<Pendaftar> findByStatusAndStatusVerifikasiOrderByDateVerifikasi(StatusRecord statusRecord, StatusRecord statusVerifikasi, Pageable pageable);


    Page<Pendaftar> findByStatusAndNamaLengkapContainingIgnoreCaseOrStatusAndUserUsernameContainingIgnoreCaseOrderByTanggalInput(StatusRecord statusRecord, String search1, StatusRecord statusRecord1, String search2, Pageable pageable);

    Page<Pendaftar> findByStatusAndUserRoleIdAndStatusVerifikasiAndNamaLengkapContainingIgnoreCaseOrStatusAndUserUsernameContainingIgnoreCaseOrderByTanggalInput(StatusRecord statusRecord,String idRole,StatusRecord verifikasi, String search1, StatusRecord statusRecord1, String search2, Pageable pageable);
    Page<Pendaftar> findByStatusAndStatusVerifikasiAndNamaLengkapContainingIgnoreCaseOrStatusAndUserUsernameContainingIgnoreCaseOrderByTanggalInput(StatusRecord statusRecord,StatusRecord statusVerifikasi, String search1, StatusRecord statusRecord1, String search2, Pageable pageable);


    Pendaftar findByUser(User user);
}
