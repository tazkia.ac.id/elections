package id.ac.tazkia.election.dao;

import id.ac.tazkia.election.entity.Pendaftar;
import id.ac.tazkia.election.entity.Sswkb;
import id.ac.tazkia.election.entity.StatusRecord;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface SswkbDao extends PagingAndSortingRepository<Sswkb, String> {
    List<Sswkb> findByStatusAndPendaftar(StatusRecord statusRecord, Pendaftar pendaftar);

    List<Sswkb> findByStatusAndPendaftarAndJenisInOrderByJenis(StatusRecord statusRecord, Pendaftar pendaftar, List<String> jenis);
}
