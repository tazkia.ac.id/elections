package id.ac.tazkia.election.dao;

import id.ac.tazkia.election.entity.Pendaftar;
import id.ac.tazkia.election.entity.RiwayatPerkerjaan;
import id.ac.tazkia.election.entity.StatusRecord;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface RiwayatPekerjaanDao extends PagingAndSortingRepository<RiwayatPerkerjaan, String> {
    List<RiwayatPerkerjaan> findByStatusAndPendaftar(StatusRecord statusRecord, Pendaftar pendaftar);
}
