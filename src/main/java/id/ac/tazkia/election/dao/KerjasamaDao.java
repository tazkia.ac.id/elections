package id.ac.tazkia.election.dao;

import id.ac.tazkia.election.entity.Kerjasama;
import id.ac.tazkia.election.entity.Pendaftar;
import id.ac.tazkia.election.entity.StatusRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface KerjasamaDao extends PagingAndSortingRepository<Kerjasama, String> {

    Page<Kerjasama> findByStatusAndPendaftar(StatusRecord statusRecord, Pendaftar pendaftar, Pageable page);

    List<Kerjasama> findByStatusAndPendaftar(StatusRecord statusRecord, Pendaftar pendaftar);

}

