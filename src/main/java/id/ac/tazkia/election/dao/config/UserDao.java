package id.ac.tazkia.election.dao.config;

import id.ac.tazkia.election.entity.User;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserDao extends PagingAndSortingRepository<User, String> {

    User findByUsername(String username);

}
