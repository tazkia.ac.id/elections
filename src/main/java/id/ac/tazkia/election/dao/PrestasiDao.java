package id.ac.tazkia.election.dao;

import id.ac.tazkia.election.entity.Pendaftar;
import id.ac.tazkia.election.entity.Prestasi;
import id.ac.tazkia.election.entity.StatusRecord;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PrestasiDao extends PagingAndSortingRepository<Prestasi, String> {
    List<Prestasi> findByStatusAndPendaftar(StatusRecord statusRecord, Pendaftar pendaftar);
}
