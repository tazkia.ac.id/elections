package id.ac.tazkia.election.dao;

import id.ac.tazkia.election.entity.Pendaftar;
import id.ac.tazkia.election.entity.StatusRecord;
import id.ac.tazkia.election.entity.SuratKesediaan;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SuratKesediaanDao extends PagingAndSortingRepository<SuratKesediaan, String> {

    SuratKesediaan findByStatusAndPendaftar(StatusRecord statusRecord, Pendaftar pendaftar);

    SuratKesediaan findByStatusAndPendaftarAndKesediaanAndNonAfiliasiPartaiAndTidakTerlibatTindakPidana(StatusRecord statusRecord, Pendaftar pendaftar, Boolean kesediaan, Boolean nonPartai, Boolean tidakPidana);

}
