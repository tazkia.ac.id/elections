package id.ac.tazkia.election.dao;

import id.ac.tazkia.election.entity.Pendaftar;
import id.ac.tazkia.election.entity.RiwayatPemimpin;
import id.ac.tazkia.election.entity.StatusRecord;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface RiwayatPemimpinDao extends PagingAndSortingRepository<RiwayatPemimpin, String> {
    List<RiwayatPemimpin> findByStatusAndPendaftar(StatusRecord statusRecord, Pendaftar pendaftar);
}
