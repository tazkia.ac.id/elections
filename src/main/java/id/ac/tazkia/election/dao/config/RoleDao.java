package id.ac.tazkia.election.dao.config;

import id.ac.tazkia.election.entity.Role;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RoleDao extends PagingAndSortingRepository<Role, String> {
}
