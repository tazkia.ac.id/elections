package id.ac.tazkia.election.dao;

import id.ac.tazkia.election.entity.Keluarga;
import id.ac.tazkia.election.entity.Pendaftar;
import id.ac.tazkia.election.entity.StatusRecord;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface KeluargaDao extends PagingAndSortingRepository<Keluarga, String> {
    List<Keluarga> findByStatusAndPendaftar(StatusRecord statusRecord, Pendaftar idPendaftar);
}
