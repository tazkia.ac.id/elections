package id.ac.tazkia.election.dao;

import id.ac.tazkia.election.entity.Pendaftar;
import id.ac.tazkia.election.entity.PresentasiCalon;
import id.ac.tazkia.election.entity.StatusRecord;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PresentasiDao extends PagingAndSortingRepository<PresentasiCalon, String> {

    PresentasiCalon findByStatusAndPendaftar(StatusRecord statusRecord, Pendaftar pendaftar);

}
