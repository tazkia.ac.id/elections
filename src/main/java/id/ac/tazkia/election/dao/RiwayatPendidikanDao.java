package id.ac.tazkia.election.dao;

import id.ac.tazkia.election.entity.Pendaftar;
import id.ac.tazkia.election.entity.RiwayatPendidikan;
import id.ac.tazkia.election.entity.StatusRecord;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface RiwayatPendidikanDao extends PagingAndSortingRepository<RiwayatPendidikan, String> {
    List<RiwayatPendidikan> findByStatusAndPendaftar(StatusRecord statusRecord, Pendaftar pendaftar);
}
