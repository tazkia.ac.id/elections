package id.ac.tazkia.election.controller;

import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import id.ac.tazkia.election.dao.OneTimePasswordDao;
import id.ac.tazkia.election.dao.config.RoleDao;
import id.ac.tazkia.election.dao.config.UserDao;
import id.ac.tazkia.election.entity.*;
import id.ac.tazkia.election.service.GmailApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.StringWriter;
import java.util.*;

@Controller
public class LoginController {

    private final Long expiryInterval = 5L * 60 * 1000;

    private final static Integer LENGTH = 6;

    @Autowired
    private OneTimePasswordDao oneTimePasswordDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private GmailApiService gmailApiService;

    @Autowired
    private MustacheFactory mustacheFactory;

    @Autowired
    private RoleDao roleDao;

    @GetMapping("/signup")
    public String signup(Model model){

        Random random = new Random();
        StringBuilder otp = new StringBuilder();
        for (int i = 0; i < LENGTH; i++){
            int randomNumber = random.nextInt(10);
            otp.append(randomNumber);
        }

        model.addAttribute("testOTP", Integer.parseInt(otp.toString().trim()));

        return "register";
    }

    @PostMapping("/signup/save")
    public String otpConf(Model model, @RequestParam String name, @RequestParam String email){

        OneTimePassword otp = new OneTimePassword();

        User user = userDao.findByUsername(email);

        if (user == null){
            Random random = new Random();
            StringBuilder oneTimePassword = new StringBuilder();
            for (int i = 0; i < LENGTH; i++){
                int randomNumber = random.nextInt(10);
                oneTimePassword.append(randomNumber);
            }

            otp.setOneTimePasswordCode(Integer.parseInt(oneTimePassword.toString().trim()));
            otp.setEmail(email);
            otp.setExpires(new Date(System.currentTimeMillis() + expiryInterval));
            otp.setStatus(StatusRecord.AKTIF);
            oneTimePasswordDao.save(otp);

            Mustache templateEmail = mustacheFactory.compile("templates/otp_confirmation.html");
            Map<String, String> data = new HashMap<>();
            data.put("otp",otp.getOneTimePasswordCode() + "");
            data.put("expires", otp.getExpires().toString());

            StringWriter output = new StringWriter();
            templateEmail.execute(output, data);

            gmailApiService.kirimEmail(
                    "One Time Password from Election Application",
                    otp.getEmail(),
                    "One Time Password verification",
                    output.toString()
            );
            return "redirect:verify?otp="+otp.getId();
        }else{
            model.addAttribute("gagal", "email sudah terdaftar");
            return "register";
        }


    }

    @GetMapping("/signup/verify")
    public String verifyOtp(Model model, @RequestParam(required = false) OneTimePassword otp){

        model.addAttribute("otp", otp);
        model.addAttribute("email", otp.getEmail());

        return "VerifyOtp";
    }

    @PostMapping("/signup/verify/otp")
    public String signVerify(Model model, @RequestParam String otp, @RequestParam String email, @RequestParam String first, @RequestParam String second, @RequestParam String third,
                             @RequestParam String fourth, @RequestParam String fifth, @RequestParam String sixth, RedirectAttributes attributes){

        String code = first+second+third+fourth+fifth+sixth;

        OneTimePassword oneTimePassword = oneTimePasswordDao.findById(otp).get();

        if (oneTimePassword.getOneTimePasswordCode() == Integer.parseInt(code) && oneTimePassword.getStatus() == StatusRecord.AKTIF) {
            oneTimePassword.setOneTimePasswordCodeVerify(Integer.parseInt(code));
            oneTimePasswordDao.save(oneTimePassword);
            attributes.addFlashAttribute("first", first);
            attributes.addFlashAttribute("second", second);
            attributes.addFlashAttribute("third", third);
            attributes.addFlashAttribute("fourth", fourth);
            attributes.addFlashAttribute("fifth", fifth);
            attributes.addFlashAttribute("sixth", sixth);

            Role calon = roleDao.findById("calonrektor").get();

            User usr = userDao.findByUsername(email);
            if (usr == null) {
                User user = new User();
                String id = UUID.randomUUID().toString();
                user.setId(id);
                user.setUsername(oneTimePassword.getEmail());
                user.setActive(true);
                user.setRole(calon);
                userDao.save(user);
            }else{
                attributes.addFlashAttribute("success", "Kode OTP berhasil.");
                return "redirect:../verify?otp="+oneTimePassword.getId();
            }

        }else if (oneTimePassword.getStatus() == StatusRecord.NONAKTIF){
            model.addAttribute("habis", "otp sudah tidak aktif");
            return verifyOtp(model, oneTimePassword);
        }else{
            model.addAttribute("gagal", "kode Otp Salah");
            return verifyOtp(model, oneTimePassword);
        }

        attributes.addFlashAttribute("success", "Kode OTP berhasil.");

        return "redirect:../verify?otp="+oneTimePassword.getId();
    }

}
