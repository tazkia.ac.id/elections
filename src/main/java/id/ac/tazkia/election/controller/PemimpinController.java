package id.ac.tazkia.election.controller;

import id.ac.tazkia.election.dao.PendaftarDao;
import id.ac.tazkia.election.dao.RiwayatPemimpinDao;
import id.ac.tazkia.election.entity.*;
import id.ac.tazkia.election.service.CurrentUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

@Controller
public class PemimpinController {
    @Autowired
    private RiwayatPemimpinDao riwayatPemimpinDao;

    private static final Logger LOGGER = LoggerFactory.getLogger(PendaftarController.class);

    @Autowired
    private PendaftarDao pendaftarDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Value("${upload.file}")
    private String uploadFile;

    @GetMapping("/form/pemimpin")
    public String riwayatPemimpin(Model model, Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        Pendaftar pendaftar = pendaftarDao.findByUser(user);
        if (pendaftar == null) {
            return "redirect:/form/pendaftar";
        }
        if (pendaftar.getStatusSubmit() == StatusRecord.SUBMITTED) {
            model.addAttribute("submit", "Data sudah dikirim, tidak bisa di perbaharui.");
        }
        model.addAttribute("list", riwayatPemimpinDao.findByStatusAndPendaftar(StatusRecord.AKTIF, pendaftar));

        model.addAttribute("pemimpins","active");

        return "form/pemimpin";
    }

    @PostMapping("/form/pemimpin")
    public String saveMemimpin(@Valid RiwayatPemimpin riwayatPemimpin,
                               @RequestParam("image") MultipartFile file,
                                Authentication authentication)throws IOException {
        User user = currentUserService.currentUser(authentication);
        Pendaftar pendaftar = pendaftarDao.findByUser(user);

        String namaFile =  file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();
        String extension = "";
        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idFile = UUID.randomUUID().toString();
        if (ukuran <= 0){

            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            if(riwayatPemimpin.getFilePendukung() == null){
                riwayatPemimpin.setFilePendukung(riwayatPemimpin.getFilePendukung());
            }
        }else{
            riwayatPemimpin.setFilePendukung(idFile + "." + extension);
            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            new File(uploadFile).mkdirs();
            File tujuan = new File(uploadFile + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);
        }

        riwayatPemimpin.setPendaftar(pendaftar);
        riwayatPemimpin.setStatus(StatusRecord.AKTIF);
        riwayatPemimpinDao.save(riwayatPemimpin);

        return "redirect:/form/pemimpin";
    }

    @PostMapping("/form/delete/pemimpin")
    public String deleteKeluarga(@RequestParam RiwayatPemimpin pemimpin){
        pemimpin.setStatus(StatusRecord.HAPUS);
        riwayatPemimpinDao.save(pemimpin);
        return "redirect:/form/pemimpin";
    }

    @GetMapping("/form/pemimpin/{pemimpin}")
    public ResponseEntity<byte[]> tampolkanFotoEmployee(@PathVariable RiwayatPemimpin pemimpin) throws Exception {
        String lokasiFile = uploadFile + File.separator + pemimpin.getFilePendukung();
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (pemimpin.getFilePendukung().toLowerCase().endsWith("jpeg") || pemimpin.getFilePendukung().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (pemimpin.getFilePendukung().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (pemimpin.getFilePendukung().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
