package id.ac.tazkia.election.controller;


import id.ac.tazkia.election.dao.PendaftarDao;
import id.ac.tazkia.election.dao.PresentasiDao;
import id.ac.tazkia.election.dao.SuratKesediaanDao;
import id.ac.tazkia.election.entity.*;
import id.ac.tazkia.election.service.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Controller
public class PendaftarController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PendaftarController.class);

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private PendaftarDao pendaftarDao;

    @Autowired
    private SuratKesediaanDao suratKesediaanDao;

    @Autowired
    private PresentasiDao presentasiDao;

    @Autowired
    @Value("${upload.foto}")
    private String uploadFolder;

    @GetMapping("/form/pendaftar")
    public void formRiwayatHidup(Model model, Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        Pendaftar pendaftar = pendaftarDao.findByStatusAndUser(StatusRecord.AKTIF, user);
        model.addAttribute("emailUser", user.getUsername());
        if (pendaftar != null){
            model.addAttribute("pendaftar", pendaftar);
            if (pendaftar.getStatusSubmit() == StatusRecord.SUBMITTED) {
                model.addAttribute("submit", "Data sudah dikirim, tidak bisa di perbaharui.");
            }
        }else {
            model.addAttribute("pendaftar", new Pendaftar());
        }
        model.addAttribute("datadiris","active");

    }

    @PostMapping("/form/pendaftar")
    public String saveFormRiwayatHidup(@ModelAttribute @Valid Pendaftar pendaftar, Authentication authentication,
                                       @RequestParam("image") MultipartFile file) throws IOException {
        User user = currentUserService.currentUser(authentication);
        pendaftar.setStatus(StatusRecord.AKTIF);

        String namaFile =  file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();
        String extension = "";
        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idFile = UUID.randomUUID().toString();
        if (ukuran <= 0){

            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            if(pendaftar.getFoto() == null){
                Pendaftar pendaftar1 = pendaftarDao.findById(pendaftar.getId()).get();
                pendaftar.setFoto(pendaftar1.getFoto());
            }
        }else{
            pendaftar.setFoto(idFile + "." + extension);
            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            new File(uploadFolder).mkdirs();
            File tujuan = new File(uploadFolder + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);
        }
        pendaftar.setUser(user);
        pendaftar.setTanggalInput(LocalDate.now());
        pendaftar.setStatusVerifikasi(StatusRecord.BELUM_VERIFIKASI);
        pendaftarDao.save(pendaftar);

        return "redirect:/form/keluarga";
    }

    @GetMapping("/form/foto{pendaftar}")
    public ResponseEntity<byte[]> tampolkanFotoEmployee(@PathVariable Pendaftar pendaftar) throws Exception {
        String lokasiFile = uploadFolder + File.separator + pendaftar.getFoto();
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (pendaftar.getFoto().toLowerCase().endsWith("jpeg") || pendaftar.getFoto().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (pendaftar.getFoto().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (pendaftar.getFoto().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/form/submit")
    public String submitData(Model model, Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Pendaftar pendaftar = pendaftarDao.findByUser(user);
        if (pendaftar == null) {
            return "redirect:/form/pendaftar";
        }
        if (pendaftar.getStatusSubmit() == StatusRecord.SUBMITTED) {
            model.addAttribute("submit", "Data sudah dikirim, tidak bisa di perbaharui.");
        }
        model.addAttribute("pendaftar", pendaftar);

        model.addAttribute("submits", "active");

        return "form/submitData";
    }

    @PostMapping("/form/submit/save")
    public String saveData(@RequestParam String pendaftar, Model model, RedirectAttributes attributes){

        Pendaftar p = pendaftarDao.findById(pendaftar).get();

        SuratKesediaan surat = suratKesediaanDao.findByStatusAndPendaftarAndKesediaanAndNonAfiliasiPartaiAndTidakTerlibatTindakPidana(StatusRecord.AKTIF, p, true, true, true);
        PresentasiCalon presentasi = presentasiDao.findByStatusAndPendaftar(StatusRecord.AKTIF, p);

        if (surat == null) {
            model.addAttribute("pendaftar", p);
            model.addAttribute("submits", "active");
            model.addAttribute("gagal", "setujui surat pernyataan!");
            return "form/submitData";
        }else if (presentasi != null) {
            if (!presentasi.getVisi().isEmpty() && !presentasi.getMisi().isEmpty() && !presentasi.getRencanaStrategis().isEmpty() && !presentasi.getActionPlan().isEmpty() && !presentasi.getLinkVideo()
                    .isEmpty()){

                p.setStatusSubmit(StatusRecord.SUBMITTED);
                p.setStatusVerifikasi(StatusRecord.BELUM_VERIFIKASI);
                p.setDateSubmit(LocalDateTime.now().plusHours(7));
                pendaftarDao.save(p);

            }else {
                model.addAttribute("pendaftar", p);
                model.addAttribute("submits", "active");
                model.addAttribute("failed", "setujui surat pernyataan!");
                return "form/submitData";
            }
        }else {
            model.addAttribute("pendaftar", p);
            model.addAttribute("submits", "active");
            model.addAttribute("failed", "setujui surat pernyataan!");
            return "form/submitData";
        }


        return "redirect:/form/submit";
    }

}
