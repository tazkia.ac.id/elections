package id.ac.tazkia.election.controller;

import id.ac.tazkia.election.dao.KeluargaDao;
import id.ac.tazkia.election.dao.PendaftarDao;
import id.ac.tazkia.election.entity.Keluarga;
import id.ac.tazkia.election.entity.Pendaftar;
import id.ac.tazkia.election.entity.StatusRecord;
import id.ac.tazkia.election.entity.User;
import id.ac.tazkia.election.service.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
public class KeluargaController {
    @Autowired
    private KeluargaDao keluargaDao;

    @Autowired
    private PendaftarDao pendaftarDao;

    @Autowired
    private CurrentUserService currentUserService;

    @GetMapping("/form/keluarga")
    public String kelurgaList(Model model, Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        Pendaftar pendaftar = pendaftarDao.findByUser(user);
        if (pendaftar == null) {
            return "redirect:/form/pendaftar";
        }

        if (pendaftar.getStatusSubmit() == StatusRecord.SUBMITTED) {
            model.addAttribute("submit", "Data sudah dikirim, tidak bisa di perbaharui.");
        }

        model.addAttribute("list", keluargaDao.findByStatusAndPendaftar(StatusRecord.AKTIF, pendaftar));
        model.addAttribute("keluargas","active");

        return "form/keluarga";
    }

    @PostMapping("/form/keluarga")
    public String saveKeluarga(@Valid Keluarga keluarga, Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        Pendaftar pendaftar = pendaftarDao.findByUser(user);


        keluarga.setStatus(StatusRecord.AKTIF);
        keluarga.setPendaftar(pendaftar);
        keluargaDao.save(keluarga);

        return "redirect:/form/keluarga";
    }

    @PostMapping("/form/delete/keluarga")
    public String deleteKeluarga(@RequestParam Keluarga keluarga){
        keluarga.setStatus(StatusRecord.HAPUS);
        keluargaDao.save(keluarga);
        return "redirect:/form/keluarga";
    }
}
