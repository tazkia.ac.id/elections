package id.ac.tazkia.election.controller;

import id.ac.tazkia.election.dao.*;
import id.ac.tazkia.election.entity.Pendaftar;
import id.ac.tazkia.election.entity.StatusRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Controller
public class VerifikasiPendaftarController {

    @Autowired
    private PendaftarDao pendaftarDao;

    @Autowired
    private KeluargaDao keluargaDao;

    @Autowired
    private RiwayatPendidikanDao riwayatPendidikanDao;

    @Autowired
    private RiwayatPekerjaanDao riwayatPekerjaanDao;

    @Autowired
    private RiwayatPemimpinDao riwayatPemimpinDao;

    @Autowired
    private PengalamanWirausahaDao pengalamanWirausahaDao;

    @Autowired
    private SswkbDao sswkbDao;

    @Autowired
    private KerjasamaDao kerjasamaDao;

    @Autowired
    private PresentasiDao presentasiDao;

    @Autowired
    private SuratKesediaanDao suratKesediaanDao;


    @GetMapping("/list/pendaftar")
    public String listPendaftar(Model model,
                                @PageableDefault(size = 20)Pageable page,
                                @RequestParam(required = false) String search){

        if (StringUtils.hasText(search)) {
            model.addAttribute("listPendaftar", pendaftarDao.findByStatusAndUserRoleIdAndStatusVerifikasiAndNamaLengkapContainingIgnoreCaseOrStatusAndUserUsernameContainingIgnoreCaseOrderByTanggalInput(StatusRecord.AKTIF, "calonrektor",StatusRecord.BELUM_VERIFIKASI, search, StatusRecord.AKTIF, search, page));
        }else{
            model.addAttribute("listPendaftar", pendaftarDao.findByStatusAndUserRoleIdAndStatusVerifikasiOrderByTanggalInput(StatusRecord.AKTIF, "calonrektor" , StatusRecord.BELUM_VERIFIKASI,page));
        }

        model.addAttribute("listpendaftars", "active");

        return "verifikasi/list_pendaftar";
    }

    @GetMapping("/list/verifikasi")
    public String listVerifikasi(Model model, @RequestParam(required = false) String search,
                                 @PageableDefault(size = 20) Pageable page){

        if (StringUtils.hasText(search)) {
            model.addAttribute("listPendaftar", pendaftarDao.findByStatusAndStatusVerifikasiAndNamaLengkapContainingIgnoreCaseOrStatusAndUserUsernameContainingIgnoreCaseOrderByTanggalInput(StatusRecord.AKTIF, StatusRecord.VERIFIKASI, search, StatusRecord.AKTIF, search, page));
        }else{
            model.addAttribute("listPendaftar", pendaftarDao.findByStatusAndStatusVerifikasiOrderByDateVerifikasi(StatusRecord.AKTIF, StatusRecord.VERIFIKASI , page));
        }

        model.addAttribute("listverifikasis", "active");

        return "verifikasi/list_verifikasi";
    }

    @GetMapping("/list/tidakTerverifikasi")
    public String listTidakTerverifikasi(Model model, @RequestParam(required = false) String search,
                                 @PageableDefault(size = 20) Pageable page){

        if (StringUtils.hasText(search)) {
            model.addAttribute("listPendaftar", pendaftarDao.findByStatusAndStatusVerifikasiAndNamaLengkapContainingIgnoreCaseOrStatusAndUserUsernameContainingIgnoreCaseOrderByTanggalInput(StatusRecord.AKTIF, StatusRecord.TIDAK_TERVERIFIKASI, search, StatusRecord.AKTIF, search, page));
        }else{
            model.addAttribute("listPendaftar", pendaftarDao.findByStatusAndStatusVerifikasiOrderByDateVerifikasi(StatusRecord.AKTIF, StatusRecord.TIDAK_TERVERIFIKASI , page));
        }

        model.addAttribute("listTidakverifikasis", "active");

        return "verifikasi/list_tidak_terverifikasi";
    }

    @GetMapping("/form/verifikasi")
    public String dataPendaftar(Model model,
                                @RequestParam(required = true) Pendaftar pendaftar){

        List<String> simposium = new ArrayList<>();
        simposium.add("Simposium");
        simposium.add("Seminar");
        simposium.add("Workshop");
        simposium.add("Konferensi");

        List<String> karyaIlmiah = new ArrayList<>();
        karyaIlmiah.add("Karya Ilmiah");

        List<String> buku = new ArrayList<>();
        buku.add("Buku");


        model.addAttribute("pendaftar", pendaftar);
        model.addAttribute("listKeluarga", keluargaDao.findByStatusAndPendaftar(StatusRecord.AKTIF, pendaftar));
        model.addAttribute("listPendidikan", riwayatPendidikanDao.findByStatusAndPendaftar(StatusRecord.AKTIF, pendaftar));
        model.addAttribute("listPekerjaan", riwayatPekerjaanDao.findByStatusAndPendaftar(StatusRecord.AKTIF, pendaftar));
        model.addAttribute("listPemimpin", riwayatPemimpinDao.findByStatusAndPendaftar(StatusRecord.AKTIF, pendaftar));
        model.addAttribute("listWirausaha", pengalamanWirausahaDao.findByStatusAndPendaftar(StatusRecord.AKTIF, pendaftar));
        model.addAttribute("listSimposium", sswkbDao.findByStatusAndPendaftarAndJenisInOrderByJenis(StatusRecord.AKTIF, pendaftar, simposium));
        model.addAttribute("listKaryaIlmiah", sswkbDao.findByStatusAndPendaftarAndJenisInOrderByJenis(StatusRecord.AKTIF, pendaftar, karyaIlmiah));
        model.addAttribute("listBuku", sswkbDao.findByStatusAndPendaftarAndJenisInOrderByJenis(StatusRecord.AKTIF, pendaftar, buku));
        model.addAttribute("listKerjaSama", kerjasamaDao.findByStatusAndPendaftar(StatusRecord.AKTIF, pendaftar));
        model.addAttribute("presentasi", presentasiDao.findByStatusAndPendaftar(StatusRecord.AKTIF, pendaftar));
        model.addAttribute("suratKesediaan", suratKesediaanDao.findByStatusAndPendaftar(StatusRecord.AKTIF, pendaftar));

        model.addAttribute("listpendaftars", "active");

        return "form/verifikasi";
    }


    @PostMapping("/form/terverifikasi")
    public String postVerifikasi(@RequestParam Pendaftar pendaftar){

        pendaftar.setStatusVerifikasi(StatusRecord.VERIFIKASI);
        pendaftar.setDateVerifikasi(LocalDateTime.now().plusHours(7));
        pendaftarDao.save(pendaftar);
        return "redirect:/list/pendaftar";
    }

    @PostMapping("/form/tidakTerverfikasi")
    public String tidakTerverifikasi(@RequestParam Pendaftar pendaftar){
        pendaftar.setStatusVerifikasi(StatusRecord.TIDAK_TERVERIFIKASI);
        pendaftar.setDateVerifikasi(LocalDateTime.now().plusHours(7));
        pendaftarDao.save(pendaftar);
        return "redirect:/list/pendaftar";
    }

}
