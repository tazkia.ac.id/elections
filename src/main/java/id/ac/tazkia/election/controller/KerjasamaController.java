package id.ac.tazkia.election.controller;

import id.ac.tazkia.election.dao.KerjasamaDao;
import id.ac.tazkia.election.dao.PendaftarDao;
import id.ac.tazkia.election.entity.*;
import id.ac.tazkia.election.service.CurrentUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

@Controller
public class KerjasamaController {

    @Autowired
    private KerjasamaDao kerjasamaDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private PendaftarDao pendaftarDao;

    @Value("${upload.file}")
    private String uploadFile;

    private static final Logger LOGGER = LoggerFactory.getLogger(PendaftarController.class);

    @GetMapping("/form/kerjasama")
    public String kerjasama(Model model, @PageableDefault(size = 10) Pageable page, Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Pendaftar pendaftar = pendaftarDao.findByUser(user);

        if (pendaftar == null) {
            return "redirect:/form/pendaftar";
        }
        if (pendaftar.getStatusSubmit() == StatusRecord.SUBMITTED) {
            model.addAttribute("submit", "Data sudah dikirim, tidak bisa di perbaharui.");
        }

//        Pendaftar pendaftar = pendaftarDao.findById(id).get();
        model.addAttribute("id", pendaftar.getId());
        model.addAttribute("list", kerjasamaDao.findByStatusAndPendaftar(StatusRecord.AKTIF, pendaftar, page));

        model.addAttribute("kerjasamas","active");

        return "form/kerjasama";

    }



    @PostMapping("/form/kerjasama/save")
    public String kerjasamaSave(@Valid Kerjasama kerjasama, @RequestParam("image") MultipartFile file, Authentication authentication) throws IOException {
        User user = currentUserService.currentUser(authentication);
        Pendaftar pendaftar = pendaftarDao.findByUser(user);

        String namaFile =  file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();
        String extension = "";
        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idFile = UUID.randomUUID().toString();
        if (ukuran <= 0){

            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            if(kerjasama.getFilePendukung() == null){
                kerjasama.setFilePendukung(kerjasama.getFilePendukung());
            }
        }else{
            kerjasama.setFilePendukung(idFile + "." + extension);
            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            new File(uploadFile).mkdirs();
            File tujuan = new File(uploadFile + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);
        }

        kerjasama.setPendaftar(pendaftar);
        kerjasama.setStatus(StatusRecord.AKTIF);
        kerjasamaDao.save(kerjasama);

        return "redirect:/form/kerjasama?id="+kerjasama.getPendaftar().getId();
    }

    @PostMapping("/form/delete/kerjasama")
    public String deleteKeluarga(@RequestParam Kerjasama kerjasama){
        kerjasama.setStatus(StatusRecord.HAPUS);
        kerjasamaDao.save(kerjasama);
        return "redirect:/form/kerjasama";
    }

    @GetMapping("/form/kerjasama/{kerjasama}")
    public ResponseEntity<byte[]> tampolkanFotoEmployee(@PathVariable Kerjasama kerjasama) throws Exception {
        String lokasiFile = uploadFile + File.separator + kerjasama.getFilePendukung();
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (kerjasama.getFilePendukung().toLowerCase().endsWith("jpeg") || kerjasama.getFilePendukung().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (kerjasama.getFilePendukung().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (kerjasama.getFilePendukung().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
