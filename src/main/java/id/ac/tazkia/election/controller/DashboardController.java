package id.ac.tazkia.election.controller;

import id.ac.tazkia.election.dao.config.UserDao;
import id.ac.tazkia.election.entity.User;
import id.ac.tazkia.election.service.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DashboardController {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private UserDao userDao;

    @GetMapping("/")
    public String dasboard(){

        return "redirect:starter";
    }

    @GetMapping("/starter")
    public String starter(Authentication authentication){

//        User user = currentUserService.currentUser(authentication);

        System.out.println("login : " + authentication);

        if (authentication == null){
            return "dashboard";
        }else{
            return "dashboard2";
        }

    }

    @GetMapping("/home")
    public String home(Authentication authentication){

        System.out.println("login : " + authentication);

        if (authentication == null){
            return "redirect:starter";
        }else{
            return "dashboard2";
        }

    }

//    @GetMapping("/dashboard")
//    public String dashboard2{
//
//    }

}
