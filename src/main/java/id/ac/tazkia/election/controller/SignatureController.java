package id.ac.tazkia.election.controller;

import id.ac.tazkia.election.dao.PendaftarDao;
import id.ac.tazkia.election.entity.Pendaftar;
import id.ac.tazkia.election.entity.User;
import id.ac.tazkia.election.service.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class SignatureController {

    @Autowired
    private PendaftarDao pendaftarDao;

    @Autowired
    private CurrentUserService currentUserService;

    @GetMapping("/signature")
    public String signature(Model model, Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        Pendaftar pendaftar = pendaftarDao.findByUser(user);

        model.addAttribute("signatures","active");

        return "form/signature";
    }

    @PostMapping("/signature/save")
    public String signatureSave(@RequestParam(value="imageBase64") String signature){

        System.out.println("file :" + signature);
        return "redirect:../signature";
    }

}
