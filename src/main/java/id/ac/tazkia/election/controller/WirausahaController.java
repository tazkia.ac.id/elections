package id.ac.tazkia.election.controller;

import id.ac.tazkia.election.dao.PendaftarDao;
import id.ac.tazkia.election.dao.PengalamanWirausahaDao;
import id.ac.tazkia.election.entity.*;
import id.ac.tazkia.election.service.CurrentUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

@Controller
public class WirausahaController {

    @Autowired
    private PendaftarDao pendaftarDao;

    @Autowired
    private PengalamanWirausahaDao wirausahaDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Value("${upload.file}")
    private String uploadFile;

    private static final Logger LOGGER = LoggerFactory.getLogger(PendaftarController.class);

    @GetMapping("/form/wirausaha")
    public String wirausaha(Model model, @PageableDefault(size = 10) Pageable page, Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Pendaftar pendaftar = pendaftarDao.findByUser(user);
        if (pendaftar == null) {
            return "redirect:/form/pendaftar";
        }
        if (pendaftar.getStatusSubmit() == StatusRecord.SUBMITTED) {
            model.addAttribute("submit", "Data sudah dikirim, tidak bisa di perbaharui.");
        }
        model.addAttribute("list", wirausahaDao.findByStatusAndPendaftar(StatusRecord.AKTIF, pendaftar, page));

        model.addAttribute("wirausahas","active");

        return "form/wirausaha";
    }

    @PostMapping("/form/wirausaha/save")
    public String saveWirausaha(@Valid PengalamanWirausaha wirausaha, @RequestParam("image") MultipartFile file, Authentication authentication) throws IOException {
        User user = currentUserService.currentUser(authentication);
        Pendaftar pendaftar = pendaftarDao.findByUser(user);

        String namaFile =  file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();
        String extension = "";
        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idFile = UUID.randomUUID().toString();
        if (ukuran <= 0){

            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            if(wirausaha.getFilePendukung() == null){
                wirausaha.setFilePendukung(wirausaha.getFilePendukung());
            }
        }else{
            wirausaha.setFilePendukung(idFile + "." + extension);
            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            new File(uploadFile).mkdirs();
            File tujuan = new File(uploadFile + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);
        }

        wirausaha.setPendaftar(pendaftar);
        wirausaha.setStatus(StatusRecord.AKTIF);
        wirausahaDao.save(wirausaha);

        return "redirect:/form/wirausaha";
    }

    @PostMapping("/form/delete/wirausaha")
    public String deleteKeluarga(@RequestParam PengalamanWirausaha wirausaha){
        wirausaha.setStatus(StatusRecord.HAPUS);
        wirausahaDao.save(wirausaha);
        return "redirect:/form/wirausaha";
    }

    @GetMapping("/form/wirausaha/{wirausaha}")
    public ResponseEntity<byte[]> tampolkanFotoEmployee(@PathVariable PengalamanWirausaha wirausaha) throws Exception {
        String lokasiFile = uploadFile + File.separator + wirausaha.getFilePendukung();
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (wirausaha.getFilePendukung().toLowerCase().endsWith("jpeg") || wirausaha.getFilePendukung().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (wirausaha.getFilePendukung().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (wirausaha.getFilePendukung().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
