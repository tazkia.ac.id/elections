package id.ac.tazkia.election.controller;

import id.ac.tazkia.election.dao.PendaftarDao;
import id.ac.tazkia.election.dao.PresentasiDao;
import id.ac.tazkia.election.entity.Pendaftar;
import id.ac.tazkia.election.entity.PresentasiCalon;
import id.ac.tazkia.election.entity.StatusRecord;
import id.ac.tazkia.election.entity.User;
import id.ac.tazkia.election.service.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class PresentasiController {

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private PresentasiDao presentasiDao;

    @Autowired
    private PendaftarDao pendaftarDao;

    @GetMapping("/form/presentasi")
    public String presentasi(Model model, Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Pendaftar pendaftar = pendaftarDao.findByUser(user);

        if (pendaftar == null) {
            return "redirect:/form/pendaftar";
        }

        model.addAttribute("idPendaftar", pendaftar.getId());
        PresentasiCalon presentasi = presentasiDao.findByStatusAndPendaftar(StatusRecord.AKTIF, pendaftar);
        if (presentasi == null) {
            model.addAttribute("presentasi", new PresentasiCalon());

        }else{
            model.addAttribute("presentasi", presentasi);
            if (pendaftar.getStatusSubmit() != null) {
                if (pendaftar.getStatusSubmit().equals(StatusRecord.SUBMITTED)) {
                    model.addAttribute("submit", "Data sudah dikirim, tidak bisa di perbaharui.");
                    System.out.println("Isi submit ada");

                }
            }
        }

        model.addAttribute("presentasis", "active");

        return "form/presentasi";
    }

    @PostMapping("/form/presentasi/save")
    public String savePresentasi(@ModelAttribute @Valid PresentasiCalon presentasiCalon){

        presentasiCalon.setStatus(StatusRecord.AKTIF);
        presentasiDao.save(presentasiCalon);

        return "redirect:/form/presentasi";
    }

}
