package id.ac.tazkia.election.controller;

import id.ac.tazkia.election.dao.PendaftarDao;
import id.ac.tazkia.election.dao.RiwayatPekerjaanDao;
import id.ac.tazkia.election.entity.*;
import id.ac.tazkia.election.service.CurrentUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

@Controller
public class PekerjaanController {

    @Autowired
    private RiwayatPekerjaanDao riwayatPekerjaanDao;

    @Autowired
    private PendaftarDao pendaftarDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Value("${upload.file}")
    private String uploadFile;

    private static final Logger LOGGER = LoggerFactory.getLogger(PendaftarController.class);

    @GetMapping("/form/pekerjaan")
    public String formPekerjaan(Model model, Authentication authentication){
        User user = currentUserService.currentUser(authentication);
        Pendaftar pendaftar = pendaftarDao.findByUser(user);
        if (pendaftar == null) {
            return "redirect:/form/pendaftar";
        }
        if (pendaftar.getStatusSubmit() == StatusRecord.SUBMITTED) {
            model.addAttribute("submit", "Data sudah dikirim, tidak bisa di perbaharui.");
        }
        model.addAttribute("list", riwayatPekerjaanDao.findByStatusAndPendaftar(StatusRecord.AKTIF, pendaftar));

        model.addAttribute("pekerjaans","active");

        return "form/pekerjaan";
    }

    @PostMapping("/form/pekerjaan")
    public String savePekerjaan(@Valid RiwayatPerkerjaan riwayatPerkerjaan, @RequestParam("image") MultipartFile file, Authentication authentication) throws IOException {
        User user = currentUserService.currentUser(authentication);
        Pendaftar pendaftar = pendaftarDao.findByUser(user);
        String namaFile =  file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();
        String extension = "";
        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));
        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }
        String idFile = UUID.randomUUID().toString();
        if (ukuran <= 0){

            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            if(riwayatPerkerjaan.getFilePendukung() == null){
                riwayatPerkerjaan.setFilePendukung(riwayatPerkerjaan.getFilePendukung());
            }
        }else{
            riwayatPerkerjaan.setFilePendukung(idFile + "." + extension);
            System.out.println("file :"+ namaAsli);
            System.out.println("Ukuran :"+ ukuran);
            new File(uploadFile).mkdirs();
            File tujuan = new File(uploadFile + File.separator + idFile + "." + extension);
            file.transferTo(tujuan);
        }
        riwayatPerkerjaan.setStatus(StatusRecord.AKTIF);
        riwayatPerkerjaan.setPendaftar(pendaftar);
        riwayatPekerjaanDao.save(riwayatPerkerjaan);
        return "redirect:/form/pekerjaan";
    }

    @PostMapping("/form/delete/pekerjaan")
    public String deleteKeluarga(@RequestParam RiwayatPerkerjaan pekerjaan){
        pekerjaan.setStatus(StatusRecord.HAPUS);
        riwayatPekerjaanDao.save(pekerjaan);
        return "redirect:/form/pekerjaan";
    }

    @GetMapping("/form/pekerjaan/{pekerjaan}")
    public ResponseEntity<byte[]> tampolkanFotoEmployee(@PathVariable RiwayatPerkerjaan pekerjaan) throws Exception {
        String lokasiFile = uploadFile + File.separator + pekerjaan.getFilePendukung();
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (pekerjaan.getFilePendukung().toLowerCase().endsWith("jpeg") || pekerjaan.getFilePendukung().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (pekerjaan.getFilePendukung().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (pekerjaan.getFilePendukung().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


}
