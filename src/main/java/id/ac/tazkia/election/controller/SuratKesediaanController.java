package id.ac.tazkia.election.controller;

import id.ac.tazkia.election.dao.PendaftarDao;
import id.ac.tazkia.election.dao.SuratKesediaanDao;
import id.ac.tazkia.election.entity.Pendaftar;
import id.ac.tazkia.election.entity.StatusRecord;
import id.ac.tazkia.election.entity.SuratKesediaan;
import id.ac.tazkia.election.entity.User;
import id.ac.tazkia.election.service.CurrentUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Controller @Slf4j
public class SuratKesediaanController {

    @Autowired
    private SuratKesediaanDao suratDao;

    @Autowired
    private PendaftarDao pendaftarDao;

    @Autowired
    private CurrentUserService currentUserService;

    @GetMapping("/form/suratKesediaan")
    public String surat(Model model, Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Pendaftar pendaftar = pendaftarDao.findByUser(user);

        if (pendaftar == null) {
            return "redirect:/form/pendaftar";
        }
        if (pendaftar.getStatusSubmit() == StatusRecord.SUBMITTED) {
            model.addAttribute("submit", "Data sudah dikirim, tidak bisa di perbaharui.");
        }

        model.addAttribute("idPendaftar", pendaftar.getId());
        model.addAttribute("pendaftar", pendaftar);

        SuratKesediaan validasi = suratDao.findByStatusAndPendaftar(StatusRecord.AKTIF, pendaftar);

        if (validasi != null){
            model.addAttribute("tgl", validasi.getTanggalKesediaan());
        }
        if (validasi == null || validasi.getKesediaan() == null){
            model.addAttribute("msg", "Data sudah terisi");
        }

        model.addAttribute("suratakhirsedia", suratDao.findByStatusAndPendaftar(StatusRecord.AKTIF, pendaftar));

        model.addAttribute("suratKesediaans", "active");

        return "form/suratKesediaan";
    }

    @PostMapping("/form/surat/kesediaan")
    public String suratKesediaan(@RequestParam(required = false) String ceklis, Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Pendaftar pendaftar = pendaftarDao.findByUser(user);

        SuratKesediaan validasi = suratDao.findByStatusAndPendaftar(StatusRecord.AKTIF, pendaftar);

        System.out.println("Cek : " + ceklis);

        if (ceklis != null) {
            if (validasi == null) {
                SuratKesediaan surat = new SuratKesediaan();
                surat.setPendaftar(pendaftar);
                surat.setKesediaan(true);
                surat.setTanggalKesediaan(LocalDateTime.now().plusHours(7));
                surat.setStatus(StatusRecord.AKTIF);
                suratDao.save(surat);
            }else{
                validasi.setKesediaan(true);
                validasi.setTanggalKesediaan(LocalDateTime.now().plusHours(7));
                suratDao.save(validasi);
            }
        }else{
            if (validasi == null) {
                SuratKesediaan surat = new SuratKesediaan();
                surat.setPendaftar(pendaftar);
                surat.setKesediaan(false);
                surat.setStatus(StatusRecord.AKTIF);
                suratDao.save(surat);
            }else{
                validasi.setKesediaan(false);
                suratDao.save(validasi);
            }
        }

        return "redirect:/form/suratNonPartai";
    }

    @GetMapping("/form/suratNonPartai")
    public String suratNonPartai(Model model, Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Pendaftar pendaftar = pendaftarDao.findByUser(user);

        if (pendaftar == null) {
            return "redirect:/form/pendaftar";
        }
        if (pendaftar.getStatusSubmit() == StatusRecord.SUBMITTED) {
            model.addAttribute("submit", "Data sudah dikirim, tidak bisa di perbaharui.");
        }

        model.addAttribute("idPendaftar", pendaftar.getId());
        model.addAttribute("pendaftar", pendaftar);
        model.addAttribute("today", LocalDate.now());

        SuratKesediaan validasi = suratDao.findByStatusAndPendaftar(StatusRecord.AKTIF, pendaftar);

        if (validasi != null){
            model.addAttribute("tgl", validasi.getTanggalNonAfiliasiPartai());
        }
        if (validasi == null || validasi.getNonAfiliasiPartai() == null){
            model.addAttribute("msg", "Data sudah terisi");
        }
        model.addAttribute("suratakhirsedia", suratDao.findByStatusAndPendaftar(StatusRecord.AKTIF, pendaftar));

        model.addAttribute("suratPartais", "active");

        return "form/suratNonAfiliasiPartai";
    }

    @PostMapping("/form/surat/nonPartai")
    public String suratNonPartai(@RequestParam(required = false) String ceklis, Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Pendaftar pendaftar = pendaftarDao.findByUser(user);

        SuratKesediaan validasi = suratDao.findByStatusAndPendaftar(StatusRecord.AKTIF, pendaftar);

        System.out.println("Cek : " + ceklis);

        if (ceklis != null) {
            if (validasi == null) {
                SuratKesediaan surat = new SuratKesediaan();
                surat.setPendaftar(pendaftar);
                surat.setNonAfiliasiPartai(true);
                surat.setStatus(StatusRecord.AKTIF);
                surat.setTanggalNonAfiliasiPartai(LocalDateTime.now().plusHours(7));
                suratDao.save(surat);
            }else{
                validasi.setNonAfiliasiPartai(true);
                validasi.setTanggalNonAfiliasiPartai(LocalDateTime.now().plusHours(7));
                suratDao.save(validasi);
            }
        }else{
            if (validasi == null) {
                SuratKesediaan surat = new SuratKesediaan();
                surat.setPendaftar(pendaftar);
                surat.setKesediaan(false);
                surat.setStatus(StatusRecord.AKTIF);
                suratDao.save(surat);
            }else{
                validasi.setKesediaan(false);
                suratDao.save(validasi);
            }
        }
        return "redirect:/form/suratPidana";
    }

    @GetMapping("/form/suratPidana")
    public String suratPidana(Model model, Authentication authentication){

        User user = currentUserService.currentUser(authentication);
        Pendaftar pendaftar = pendaftarDao.findByUser(user);

        if (pendaftar == null) {
            return "redirect:/form/pendaftar";
        }
        if (pendaftar.getStatusSubmit() == StatusRecord.SUBMITTED) {
            model.addAttribute("submit", "Data sudah dikirim, tidak bisa di perbaharui.");
        }

        model.addAttribute("idPendaftar", pendaftar.getId());
        model.addAttribute("pendaftar", pendaftar);
        model.addAttribute("today", LocalDate.now());

        SuratKesediaan validasi = suratDao.findByStatusAndPendaftar(StatusRecord.AKTIF, pendaftar);

        if (validasi != null){
            model.addAttribute("tgl", validasi.getTanggalTidakTerlibatTindakPidana());
        }
        if (validasi == null || validasi.getTidakTerlibatTindakPidana() == null){
            model.addAttribute("msg", "Data sudah terisi");
        }

        model.addAttribute("suratakhirsedia", suratDao.findByStatusAndPendaftar(StatusRecord.AKTIF, pendaftar));

        model.addAttribute("suratPidanas", "active");

        return "form/suratTidakTerlibatPidana";
    }

    @PostMapping("/form/surat/pidana")
    public String suratPidana(@RequestParam(required = false) String ceklis, Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Pendaftar pendaftar = pendaftarDao.findByUser(user);

        SuratKesediaan validasi = suratDao.findByStatusAndPendaftar(StatusRecord.AKTIF, pendaftar);

        System.out.println("Cek : " + ceklis);

        if (ceklis != null) {
            if (validasi == null) {
                SuratKesediaan surat = new SuratKesediaan();
                surat.setPendaftar(pendaftar);
                surat.setTidakTerlibatTindakPidana(true);
                surat.setStatus(StatusRecord.AKTIF);
                surat.setTanggalTidakTerlibatTindakPidana(LocalDateTime.now().plusHours(7));
                suratDao.save(surat);
            }else{
                validasi.setTidakTerlibatTindakPidana(true);
                validasi.setTanggalTidakTerlibatTindakPidana(LocalDateTime.now().plusHours(7));
                suratDao.save(validasi);
            }
        }else{
            if (validasi == null) {
                SuratKesediaan surat = new SuratKesediaan();
                surat.setPendaftar(pendaftar);
                surat.setKesediaan(false);
                surat.setStatus(StatusRecord.AKTIF);
                suratDao.save(surat);
            }else{
                validasi.setKesediaan(false);
                suratDao.save(validasi);
            }
        }
        return "redirect:/form/submit";
    }

}
