package id.ac.tazkia.election.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Entity
public class Pendaftar {
    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String namaLengkap;

    private String jenisKelamin;

    private String agama;

    private String tempatLahir;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalLahir;

    private String statusPerkawinan;

    private String alamatSaatIni;
    private String alamatTetap;

    private String nomorTelpon;

    private String emailSatu;
    private String emailDua;

    private String noKtp;

    private String npwp;
    private String jenjangJabatanAkademis;

    private String asalInstitusi;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalInput;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private User user;

    private String foto;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusSubmit;

    private LocalDateTime dateSubmit;

    @Enumerated(EnumType.STRING)
    private StatusRecord statusVerifikasi;

    private LocalDateTime dateVerifikasi;

}
