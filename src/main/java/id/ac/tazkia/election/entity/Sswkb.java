package id.ac.tazkia.election.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Data
public class Sswkb {
    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_pendaftar")
    private Pendaftar pendaftar;

    private String nama;
    private String lama;
    private String tempat;
    private String tahun;
    private String penulis;
    private String jumlahHalaman;
    private String tahunTerbit;
    private String penerbit;
    private String keterangan;


    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String jenis;

    private String filePendukung;
}
