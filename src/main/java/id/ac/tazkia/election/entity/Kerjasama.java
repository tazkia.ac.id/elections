package id.ac.tazkia.election.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Data
public class Kerjasama {
    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_pendaftar")
    private Pendaftar pendaftar;

    private String namaKegiatan;
    private String mitraKerjasama;
    private String bulanMulai;
    private String bulanAkhir;
    private String tahunMulai;
    private String tahunAkhir;

    private String bidangKerjasama;

    private String referensi;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private String filePendukung;
}
