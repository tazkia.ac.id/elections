package id.ac.tazkia.election.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Data
public class RiwayatPemimpin {
    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String namaJabatan;
    private String bulanMulaiJabatan;
    private String bulanAkhirJabatan;
    private String tahunMulaiJabatan;
    private String tahunAkhirJabatan;
    private String namaInstansi;
    private String jabatan;
    private String keterangan;
    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;
    private String jenis;

    @ManyToOne
    @JoinColumn(name = "id_pendaftar")
    private Pendaftar pendaftar;

    private String filePendukung;


}
