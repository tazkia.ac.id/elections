package id.ac.tazkia.election.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@Entity
public class PengalamanWirausaha {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String namaWirausaha;
    private String bidang;
    private String bulanMulai;
    private String bulanAkhir;
    private String tahunMulai;
    private String tahunAkhir;

    private String lokasi;
    private String jumlahKaryawan;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    @ManyToOne
    @JoinColumn(name = "id_pendaftar")
    private Pendaftar pendaftar;

    private String filePendukung;
}
