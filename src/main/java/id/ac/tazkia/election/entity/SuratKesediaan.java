package id.ac.tazkia.election.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
public class SuratKesediaan {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_pendaftar")
    private Pendaftar pendaftar;

    private Boolean kesediaan;

    private Boolean nonAfiliasiPartai;
    private Boolean tidakTerlibatTindakPidana;
    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.AKTIF;

    private LocalDateTime tanggalKesediaan;

    private LocalDateTime tanggalNonAfiliasiPartai;

    private LocalDateTime tanggalTidakTerlibatTindakPidana;

}
